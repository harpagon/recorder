package harpagons.recorderapp

import android.Manifest
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.MediaRecorder
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.activity_main.*
import urbanstew.RehearsalAssistant.RehearsalAudioRecorder
import java.io.File
import java.io.IOException


class MainActivity : AppCompatActivity() {

	private val root = Environment.getExternalStorageDirectory().absolutePath
	private val bgPath = "$root/Pictures/.recorder_bg.jpg"
	private val path = "$root/recorded"
	private val destination = File(path)
	private var recorder = RehearsalAudioRecorder(
		true,
		MediaRecorder.AudioSource.MIC,
		44100,
		AudioFormat.CHANNEL_IN_MONO,
		AudioFormat.ENCODING_PCM_16BIT
	)
	private var isReversed = false

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		if (ContextCompat.checkSelfPermission(
				this,
				Manifest.permission.RECORD_AUDIO
			) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
				this,
				Manifest.permission.WRITE_EXTERNAL_STORAGE
			) != PackageManager.PERMISSION_GRANTED
		) {
			val permissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				arrayOf(
					Manifest.permission.RECORD_AUDIO,
					Manifest.permission.WRITE_EXTERNAL_STORAGE,
					Manifest.permission.READ_EXTERNAL_STORAGE
				)
			} else {
				arrayOf(
					Manifest.permission.RECORD_AUDIO,
					Manifest.permission.WRITE_EXTERNAL_STORAGE
				)
			}
			ActivityCompat.requestPermissions(this, permissions, 0)
		}
		// if this fails, this will be super easy,
		// barely an inconvenience
		BG.set(parentLayout, bgPath, resources)
		recButton.setOnClickListener { onButtonClick() }
	}

	override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
		when (keyCode) {
			KeyEvent.KEYCODE_VOLUME_DOWN -> {
				if (!(isReversed or fileNameEdit.isEnabled)) {
					isReversed = true
					signalRecorder()
					signalButton()
				}
				return true
			}
			KeyEvent.KEYCODE_VOLUME_UP -> {
				if (isReversed or isRecording()) return true
				fileNameEdit.isEnabled = !fileNameEdit.isEnabled
				if (fileNameEdit.isEnabled) {
					fileNameEdit.setBackgroundResource(R.color.fileNameBG)
					Keyboard.showSoftKeyboard(applicationContext, fileNameEdit)
				} else {
					fileNameEdit.setBackgroundResource(R.color.fileNameTranspBG)
					Keyboard.hideKeyboard(applicationContext, fileNameEdit)
				}
				return true
			}
			KeyEvent.KEYCODE_BACK -> {
				// we block this so we don't accidentally kill the app
				// during recording
				return true
			}
			else -> {
				return super.onKeyDown(keyCode, event)
			}
		}
	}

	override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
		when (keyCode) {
			KeyEvent.KEYCODE_VOLUME_DOWN -> {
				if (isReversed and !fileNameEdit.isEnabled) {
					isReversed = false
					signalRecorder()
					signalButton()
				}
				return true
			}
			KeyEvent.KEYCODE_VOLUME_UP -> {
				return true
			}
			KeyEvent.KEYCODE_BACK -> {
				return true
			}
		}
		return super.onKeyUp(keyCode, event)
	}

	private fun onButtonClick() {
		if (!(isReversed or fileNameEdit.isEnabled)) {
			signalRecorder()
			signalButton()
		}
	}

	private fun signalRecorder() {
		try {
			if (isRecording()) {
				recorder.reset()
			} else {
				if (!destination.exists()) {
					destination.mkdir()
				}
				recorder.setOutputFile("$path/${findAvailableName()}")
				recorder.prepare()
				recorder.start()
			}
		} catch (e: IllegalStateException) {
			e.printStackTrace()
		} catch (e: IOException) {
			e.printStackTrace()
		}
	}

	private fun signalButton() {
		val color: Int =
			if (isRecording()) {
				if (isReversed) {
					R.color.recordingQuick
				} else {
					R.color.recordingActive
				}
			} else {
				if (isReversed) {
					R.color.recordingBreak
				} else {
					R.color.recordingInactive
				}
			}
		recButton.setBackgroundColor(
			ResourcesCompat.getColor(
				resources,
				color,
				null
			)
		)
	}

	private fun isRecording(): Boolean {
		return recorder.state == RehearsalAudioRecorder.State.RECORDING
	}

	private fun formName(prefix: String, i: Int): String {
		return if (prefix == "")
			"$i.wav"
		else
			"$prefix-$i.wav"
	}

	private fun alreadyExists(name: String): Boolean {
		destination.walk().forEach {
			if (name == it.name) return true
		}
		return false
	}

	private fun findAvailableName(): String {
		val prefix = fileNameEdit.text.toString()
		var i = 1
		var name = formName(prefix, i)
		while (alreadyExists(name)) {
			i += 1
			name = formName(prefix, i)
		}
		return name
	}
}
