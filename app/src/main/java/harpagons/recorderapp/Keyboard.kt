package harpagons.recorderapp

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

object Keyboard {
	fun hideKeyboard(context: Context, view: View) {
		val imm =
			context.getSystemService(Context.INPUT_METHOD_SERVICE)
					as InputMethodManager
		imm.hideSoftInputFromWindow(
			view.windowToken,
			InputMethodManager.HIDE_IMPLICIT_ONLY
		)
	}

	fun showSoftKeyboard(context: Context, view: View) {
		if (view.requestFocus()) {
			val imm =
				context.getSystemService(Context.INPUT_METHOD_SERVICE)
						as InputMethodManager
			imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
		}
	}
}